//
//  GraphQLNetworkingServiceTests.swift
//  GraphQL-ARKit-ExperimentsTests
//
//  Created by Skyler Bellwood on 6/21/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import XCTest
@testable import GraphQL_ARKit_Experiments

class GraphQLNetworkingServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testCanCreateApolloClient() throws {
        let sut = GraphQLNetworkingService()
        
        XCTAssertNotNil(sut.apolloClient)
    }
    
    func testCanFetchFilms() throws {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: FilmListQuery()) { result in
            
            switch result {
            case .success(let queryResult):
                XCTAssertNotNil(queryResult.data)
            default:
                XCTFail()
            }
        }
    }
    
    func testCanFetchCharacterListByFilm() throws {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterListByFilmQuery(id: "cj0nxmy3fga5s01148gf8iy3c")) { result in
            
            switch result {
            case .success(let queryResult):
                XCTAssertNotNil(queryResult.data)
                XCTAssertNil(queryResult.errors)
            default:
                XCTFail()
            }
        }
    }
    
    func testCanFailToFetchCharacterListByFilm() throws {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterListByFilmQuery(id: "")) { result in
            
            switch result {
            case .success(let queryResult):
                XCTAssertNil(queryResult.data)
                XCTAssert(queryResult.errors!.count > 0)
            default:
                XCTFail()
            }
        }
    }
    
    func testCanFetchCharacterDetail() throws {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterDetailQuery(id: "cj0nv9p8yewci0130wjy4o5fa")) { result in
            
            switch result {
            case .success(let queryResult):
                XCTAssertNotNil(queryResult.data)
                XCTAssertNil(queryResult.errors)
            default:
                XCTFail()
            }
             
        }
    }
    
    func testCanFailToFetchCharacterDetail() throws {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterDetailQuery(id: "")) { result in
            switch result {
            case .success(let queryResult):
                XCTAssertNil(queryResult.data)
                XCTAssert(queryResult.errors!.count > 0)
            default:
                XCTFail()
            }
        }
    }

}
