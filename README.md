# GraphQL-ARKit-Experiments
## Author: Skyler Bellwood
This is an exploratory project utilizing two technologies to which I am new: GraphQL and ARKit. The GraphQL portion of the project utilizes a third party library, [Apollo][1], to interact with a publicly available [GraphQL wrapper][2] for the Star Wars API. The ARKit portion of the project utilizes Apple’s ARKit to track a user for the purpose of finding their range of motion. Currently the app can calculate the range of motion for an individual’s left and right arms. 
## How to Run Project
This project uses CocoaPods to access the third party Apollo library. After cloning the project to your machine, navigate to the root of the project and run `pod install` to download the dependency.

After running `pod install` make sure that you open the project using the generated `GraphQL-ARKit-Experiments.xcworkspace` file, rather than the usual `GraphQL-ARKit-Experiments.xcodeproj` that one usually opens in Xcode.

Since this project utilizes ARKit, Apple will show build errors if the build is pointed to a simulator device. To remedy this, simply point the build to `Generic iOS Device` or plug a device in and point the build to your device. If you point the build to a personal device, you will need a active Apple Developer account. Once you point the build toward your personal device, head over to the Signing & Capabilities tab in the project editor and select your account from the "Team" dropdown. If you need more information on how to change the build destination, visit Apple’s documentation [here][3]. After correctly deciding on a build destination, you can run the app through Xcode or by using the keyboard shortcut: `⌘R`
## How to Run Tests
After correctly deciding on a build destination, you can also run all of the unit tests in the project. You can do this manually by going to the `GraphQL-ARKit-ExperimentsTests` directory and selecting a test class to run OR you can run all tests from within Xcode by using the keyboard shortcut: `⌘U`
## Running in the simulator
If the app must be run in the simulator, you could replace the contents of `ARKitViewController` with the following class:
```swift
class ARKitViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
```
Please note that this will break the ARKit tab of the app. Selecting this tab while running in the simulator will cause an app crash.

[1]:	https://github.com/apollographql/apollo-ios
[2]:	https://swapi.graph.cool
[3]:	https://developer.apple.com/documentation/xcode/running_your_app_in_the_simulator_or_on_a_device
