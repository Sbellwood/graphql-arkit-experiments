//
//  CharacterDetail.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/20/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit

class CharacterDetail: UIView {

    @IBOutlet var characterNameLabel: UILabel!
    @IBOutlet var birthInfoLabel: UILabel!
    @IBOutlet var filmsLabel: UILabel!
    @IBOutlet var vehiclesLabel: UILabel!
    @IBOutlet var starshipsLabel: UILabel!

}
