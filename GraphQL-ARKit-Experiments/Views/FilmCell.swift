//
//  FilmCell.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/20/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit

class FilmCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var title: UILabel!
    
}
