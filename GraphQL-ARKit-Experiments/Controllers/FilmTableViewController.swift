//
//  GraphQLTableViewController.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/19/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit
import Apollo

class FilmTableViewController: UITableViewController {
    
    // MARK: - Properties
    
    // filmId is set by the parent view controller, when the property is set the `loadFilm` method is triggered
    var filmID: GraphQLID? {
        didSet {
            self.loadFilm()
        }
    }
    var characters = [CharacterListByFilmQuery.Data.Film.Character]()
    private let reuseIdentifier = "reuseIdentifier"
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        let character = characters[indexPath.row]
        let species = character.species?.first

        cell.textLabel?.text = character.name
        cell.detailTextLabel?.text = "Species: \(species?.name ?? "Unknown")"
        
        return cell
    }
    
    // didSelectRowAt fires off a deselectRow call on the same indexPath to stop the UI from persisting a highlighted cell on the table view
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let indexPath = self.tableView.indexPathForSelectedRow else {
            return
        }
        
        if let destination = segue.destination as? CharacterDetailViewController {
            destination.characterID = self.characters[indexPath.row].id
        }

    }
    
    // MARK: - Helper functions
    
    private func loadFilm() {
        guard let filmID = filmID else {
            self.presentErrorAlert(title: "Navigation Error", message: "No such film ID exists")
            return
        }
        
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterListByFilmQuery(id: filmID)) { [weak self] result in
            
            // guard statement to gracefully deal with the Optional `weak self`
            guard let self = self else {
                return
            }
            
            // defer creates a stack of actions that will always be run before the current scope ends
            defer {
                self.tableView.reloadData()
            }
            
            switch result {
            case .success(let queryResult):
            
                if let characters = queryResult.data?.film?.characters {
                    self.characters.append(contentsOf: characters.compactMap { $0 })
                }
                
                if let errors = queryResult.errors {
                    let title = errors.count > 1 ? "Query Errors" : "Query Error"
                    
                    let message = errors.map {
                        $0.localizedDescription
                    }.joined(separator: "\n")
                    
                    self.presentErrorAlert(title: title, message: message)
                }
                
            case .failure(let error):
                self.presentErrorAlert(title: "Networking service error", message: error.localizedDescription)
            }
        }
    }
}
