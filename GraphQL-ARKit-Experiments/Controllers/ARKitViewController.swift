//
//  ARKitViewController.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/19/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit
import RealityKit
import ARKit

/*
 This controller governs the ARKit scene. Due to simulator limitations, if the build is pointed towards a simulator this file will generate build errors. To avoid build errors, simply point this codebase at "Generic iOS Device" or a physical device. Refer to the README for further instructions on running this app in the simulator.
 */
class ARKitViewController: UIViewController, ARSessionDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // MARK: - Enums
    
    // The `CaseIterable` here allows us to access the `allCases` property of an enum
    enum RangeOfMotionTarget: String, CaseIterable {
        case leftArm = "Left Arm"
        case rightArm = "Right Arm"
    }
    
    // MARK: - Properties
    
    @IBOutlet var arView: ARView!
    @IBOutlet weak var measurementTargetLabel: UILabel!
    @IBOutlet weak var degreeOutputLabel: UILabel!
    @IBOutlet weak var measureMentTriggerButton: UIButton!
    
    // these two properties store the ARKit's body tracking point of interest so that we can calculate range of motion between the two anchors
    var firstAnchor: ARBodyAnchor?
    var lastAnchor: ARBodyAnchor?
    
    // Convenience constant that stores the relationship between radians and degrees
    let radiansToDegreesMultiplier = Float(180.0 / Double.pi)
    
    let picker = UIPickerView()
    
    // defaulted properties of state for the scene
    var isMeasuringRangeOfMotion = false
    var rangeOfMotionTarget: RangeOfMotionTarget = .leftArm

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        arView.session.delegate = self
        
        // initial configuration of view elements
        measurementTargetLabel.text = "Range of motion for \(rangeOfMotionTarget.rawValue):"
        degreeOutputLabel.text = ""
        
        // this statement allows us to throw up an error alert if the device accessing this feature is not supported
        guard ARBodyTrackingConfiguration.isSupported else {
            self.presentErrorAlert(title: "Unsupported Device", message: "This feature is not supported on your device. Please try again with a device that has an A12 chip or greater.")
            return
        }
        
        let configuration = ARBodyTrackingConfiguration()
        arView.session.run(configuration)
    }
    
    // MARK: - ARKit
    
    // This method is called when an ARKit session updates the system with a new list of anchors. As briefly mentioned earlier, an anchor is a point of interest that contains information about the spacial positioning of the tracked object(s)
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        for anchor in anchors {
            
            // we are only interested in anchors that are of the specific type: ARBodyAnchor
            guard let bodyAnchor = anchor as? ARBodyAnchor else {
                continue
            }
            
            // we do not want to do any calculations on the anchors if the first anchor is nil. If we somehow get here and the first anchor is nil, the else block will end the current measurement session.
            if let firstAnchor = firstAnchor {
                lastAnchor = bodyAnchor
                
                guard let lastAnchor = lastAnchor else {
                    continue
                }
                // naming convention here is based on the geometry point and method of generation, see calculation of angle section below for more details on calculation logic. MT refers to the `modelTransform` method of generating the 4x4 matrix
                var p1MT: simd_float4x4?
                var p2MT: simd_float4x4?
                var p3MT: simd_float4x4?
                
                var angle: Float
                // different range of motion target cases will demand different joints to reference for angle calculations
                switch rangeOfMotionTarget {
                case .leftArm:
                    p1MT = firstAnchor.skeleton.modelTransform(for: .leftShoulder)
                    p2MT = firstAnchor.skeleton.modelTransform(for: .leftHand)
                    p3MT = lastAnchor.skeleton.modelTransform(for: .leftHand)
                    
                    guard let p1MT = p1MT,
                        let p2MT = p2MT,
                        let p3MT = p3MT else {
                            continue
                    }
                    
                    // Calculation of angle:
                    // angle (in radians) = atan2(p3.y - p1.y, p3.x - p1.x) - atan2(p2.y - p1.y, p2.x - p1.x)
                    angle = atan2(p3MT.columns.1.y - p1MT.columns.1.y, p3MT.columns.1.x - p1MT.columns.1.x)
                        - atan2(p2MT.columns.1.y - p1MT.columns.1.y, p2MT.columns.1.x - p1MT.columns.1.x)
                case .rightArm:
                    p1MT = firstAnchor.skeleton.modelTransform(for: .rightShoulder)
                    p2MT = firstAnchor.skeleton.modelTransform(for: .rightHand)
                    p3MT = lastAnchor.skeleton.modelTransform(for: .rightHand)
                    
                    guard let p1MT = p1MT,
                        let p2MT = p2MT,
                        let p3MT = p3MT else {
                            continue
                    }
                    
                    angle = atan2(p3MT.columns.1.y - p1MT.columns.1.y, p3MT.columns.1.x - p1MT.columns.1.x)
                        - atan2(p2MT.columns.1.y - p1MT.columns.1.y, p2MT.columns.1.x - p1MT.columns.1.x)
                    
                    // invert the angle to mirror the left arm angle
                    angle = (Float(Double.pi * 2) - angle) - Float(Double.pi * 4)
                }
                                   
                var resultInRadians: Float
                
                // if the resulting angle is negative, add 2π to the result
                if angle < 0 {
                    resultInRadians = angle + Float(2 * Double.pi)
                } else {
                    resultInRadians = angle
                }
                
                // change radian unit to a more familiar degree unit
                let resultInDegrees = resultInRadians * radiansToDegreesMultiplier
                degreeOutputLabel.text = "\(Int(resultInDegrees))°"
            } else {
                endMeasurementSession()
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func measurementTriggerTouched(_ sender: Any) {
        let session = arView.session
                
        // checking state on the scene to decide how to handle the button tap
        if isMeasuringRangeOfMotion {
            endMeasurementSession()
        } else {
            // this guard ensures that the ARKit session is tracking a body before starting a measurement session
            guard let anchor = session.currentFrame?.anchors.last as? ARBodyAnchor else {
                return
            }
            
            beginMeasurementSession(with: anchor)
        }
    }
    
    @IBAction func changeMeasurementTargetTouched(_ sender: Any) {
        
        // we should close down any measurement session before changing the measurement target
        endMeasurementSession()
        
        // setting up the PickerView for logic and display
        picker.delegate = self
        picker.autoresizingMask = .flexibleWidth
        picker.contentMode = .center
        picker.frame = CGRect.init(x: UIScreen.main.bounds.size.width / 2 - 150, y: UIScreen.main.bounds.size.height / 2 - 150, width: 300, height: 300)
        picker.backgroundColor = UIColor.darkGray
        picker.layer.cornerRadius = 5.0
        self.view.addSubview(picker)
    }
    
    // MARK: - PickerView
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return RangeOfMotionTarget.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return RangeOfMotionTarget.allCases[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.rangeOfMotionTarget = RangeOfMotionTarget.allCases[row]
        measurementTargetLabel.text = "Range of motion for \(rangeOfMotionTarget.rawValue):"
        self.picker.removeFromSuperview()
    }
    
    // MARK: - Private Helper Functions
    
    private func endMeasurementSession() {
        isMeasuringRangeOfMotion = false
        measureMentTriggerButton.setTitle("Start Measurement", for: .normal)
        degreeOutputLabel.text = ""
        firstAnchor = nil
        lastAnchor = nil
    }
    
    private func beginMeasurementSession(with anchor: ARBodyAnchor) {
        isMeasuringRangeOfMotion = true
        measureMentTriggerButton.setTitle("End Measurement", for: .normal)
        firstAnchor = anchor
    }
}
