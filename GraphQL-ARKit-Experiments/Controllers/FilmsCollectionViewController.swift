//
//  GraphQLCollectionViewController.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/20/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit

class FilmsCollectionViewController: UICollectionViewController {

    // MARK: - Properties
    
    // Array of Film objects from the nested GraphQL query
    var films = [FilmListQuery.Data.AllFilm]()
    private let reuseIdentifier = "filmCell"

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadFilms()
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.films.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as? FilmCell else {
            return UICollectionViewCell()
        }
    
        let film = self.films[indexPath.row]
        cell.title.text = "Episode \(film.episodeId): \(film.title)"
        cell.imageView.image = UIImage(named: "episode\(film.episodeId)-icon")
        
        return cell
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let indexPath = self.collectionView.indexPathsForSelectedItems?.first else {
            return
        }
        
        if let destination = segue.destination as? FilmTableViewController {
            destination.filmID = self.films[indexPath.row].id
        }

    }

    // MARK: - Helper functions
    
    private func loadFilms() {
        GraphQLNetworkingService.shared.apolloClient.fetch(query: FilmListQuery()) { [weak self] result in
            
            // guard statement to gracefully deal with the Optional `weak self`
            guard let self = self else {
                return
            }
            
            // defer creates a stack of actions that will always be run before the current scope ends
            defer {
                self.collectionView.reloadData()
            }
            
            switch result {
            case .success(let queryResult):
                
                // compactMap is useful here as the film could be nil. compactMap transforms then unwraps optionals, discarding any that are nil.
                if let allFilms = queryResult.data?.allFilms {
                    self.films.append(contentsOf: allFilms.compactMap { $0 })
                }
                
                // it's possible to receive a query that has data and errors at the same time, thus checking for both is necessary
                if let errors = queryResult.errors {
                    let title = errors.count > 1 ? "Query Errors" : "Query Error"
                    
                    // compactMap (or flatMap) is not necessary here, as `errors` is not optional
                    let message = errors.map {
                        $0.localizedDescription
                    }.joined(separator: "\n")
                    
                    self.presentErrorAlert(title: title, message: message)
                }
            case .failure(let error):
                self.presentErrorAlert(title: "Networking service error", message: error.localizedDescription)
            }
        }
    }
}
