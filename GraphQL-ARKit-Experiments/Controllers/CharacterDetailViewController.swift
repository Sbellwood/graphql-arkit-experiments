//
//  GraphQLDetailViewController.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/19/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import UIKit
import Apollo

class CharacterDetailViewController: UIViewController {
    
    // MARK: - Properties
    
    var characterID: GraphQLID? {
        didSet {
            loadCharacter()
        }
    }
    var character: CharacterDetailQuery.Data.Person?

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Helper functions
    
    private func loadCharacter() {
        
        guard let characterID = characterID else {
            self.presentErrorAlert(title: "Navigation Error", message: "No such character ID exists")
            return
        }
        
        GraphQLNetworkingService.shared.apolloClient.fetch(query: CharacterDetailQuery(id: characterID)) { [weak self] result in
            
            guard let self = self else {
                return
            }
            
            // defer will configure the view after the fetch and handling has finished
            defer {
                self.configureView()
            }
            
            switch result {
            case .success(let queryResult):
                self.character = queryResult.data?.person
            
                if let errors = queryResult.errors {
                    let title = errors.count > 1 ? "Query Errors" : "Query Error"
                    
                    let message = errors.map {
                        $0.localizedDescription
                    }.joined(separator: "\n")
                    
                    self.presentErrorAlert(title: title, message: message)
                }
            
            case .failure(let error):
                self.presentErrorAlert(title: "Networking service error", message: error.localizedDescription)
            }
        }
    }
    
    private func configureView() {
        
        guard let view = self.view as? CharacterDetail else {
            return
        }
        
        // compact map returning a String that combines two properties of the films object
        let episodes = character?.films?.compactMap { episode -> String in
            return "\(episode.episodeId): \(episode.title)"
        }.joined(separator: "\n")
        
        let vehicles = character?.vehicles?.compactMap { $0.name }.joined(separator: "\n")
        let starships = character?.starships?.compactMap { $0.name }.joined(separator: "\n")
        
        // configuring the view's labels
        view.characterNameLabel.text = character?.name
        view.birthInfoLabel.text = "\(character?.birthYear ?? "unknown") on \(character?.homeworld?.name ?? "unknown")"
        view.filmsLabel.text = episodes ?? "Does not appear in any films"
        view.vehiclesLabel.text = vehicles
        view.starshipsLabel.text = starships
        
    }
}
