//
//  GraphQLNetworkingService.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/19/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import Apollo

/*
 This class creates a singleton of the Apollo library client object. The client requires a URL object, created by supplying the address to the `URL` init method.
 
 The client can then be accessed anywhere throughout the app by using a call such as `GraphQLNetworkingService.shared.apolloClient`
 */
class GraphQLNetworkingService {
    
    private let urlString = "https://swapi.graph.cool"
    
    // Singleton instance
    static let shared = GraphQLNetworkingService()
    
    // private(set) describes that this variable cannot be settable by any other entity
    // lazy describes that this variable is not created until it is needed
    private(set) lazy var apolloClient = ApolloClient(url: URL(string: urlString)!)
}
