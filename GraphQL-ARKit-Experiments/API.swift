// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class CharacterDetailQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CharacterDetail($id: ID!) {
      Person(id: $id) {
        __typename
        birthYear
        films {
          __typename
          title
          episodeId
        }
        homeworld {
          __typename
          name
        }
        name
        starships {
          __typename
          name
        }
        vehicles {
          __typename
          name
        }
      }
    }
    """

  public let operationName: String = "CharacterDetail"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("Person", arguments: ["id": GraphQLVariable("id")], type: .object(Person.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(person: Person? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Person": person.flatMap { (value: Person) -> ResultMap in value.resultMap }])
    }

    public var person: Person? {
      get {
        return (resultMap["Person"] as? ResultMap).flatMap { Person(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Person")
      }
    }

    public struct Person: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Person"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("birthYear", type: .scalar(String.self)),
        GraphQLField("films", type: .list(.nonNull(.object(Film.selections)))),
        GraphQLField("homeworld", type: .object(Homeworld.selections)),
        GraphQLField("name", type: .nonNull(.scalar(String.self))),
        GraphQLField("starships", type: .list(.nonNull(.object(Starship.selections)))),
        GraphQLField("vehicles", type: .list(.nonNull(.object(Vehicle.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(birthYear: String? = nil, films: [Film]? = nil, homeworld: Homeworld? = nil, name: String, starships: [Starship]? = nil, vehicles: [Vehicle]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Person", "birthYear": birthYear, "films": films.flatMap { (value: [Film]) -> [ResultMap] in value.map { (value: Film) -> ResultMap in value.resultMap } }, "homeworld": homeworld.flatMap { (value: Homeworld) -> ResultMap in value.resultMap }, "name": name, "starships": starships.flatMap { (value: [Starship]) -> [ResultMap] in value.map { (value: Starship) -> ResultMap in value.resultMap } }, "vehicles": vehicles.flatMap { (value: [Vehicle]) -> [ResultMap] in value.map { (value: Vehicle) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      /// The birth year of the person, using the in-universe standard of BBY or ABY -
      /// Before the Battle of Yavin or After the Battle of Yavin. The Battle of Yavin
      /// is a battle that occurs at the end of Star Wars episode IV: A New Hope.
      public var birthYear: String? {
        get {
          return resultMap["birthYear"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "birthYear")
        }
      }

      public var films: [Film]? {
        get {
          return (resultMap["films"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Film] in value.map { (value: ResultMap) -> Film in Film(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Film]) -> [ResultMap] in value.map { (value: Film) -> ResultMap in value.resultMap } }, forKey: "films")
        }
      }

      public var homeworld: Homeworld? {
        get {
          return (resultMap["homeworld"] as? ResultMap).flatMap { Homeworld(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "homeworld")
        }
      }

      /// The name of this person.
      public var name: String {
        get {
          return resultMap["name"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var starships: [Starship]? {
        get {
          return (resultMap["starships"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Starship] in value.map { (value: ResultMap) -> Starship in Starship(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Starship]) -> [ResultMap] in value.map { (value: Starship) -> ResultMap in value.resultMap } }, forKey: "starships")
        }
      }

      public var vehicles: [Vehicle]? {
        get {
          return (resultMap["vehicles"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Vehicle] in value.map { (value: ResultMap) -> Vehicle in Vehicle(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Vehicle]) -> [ResultMap] in value.map { (value: Vehicle) -> ResultMap in value.resultMap } }, forKey: "vehicles")
        }
      }

      public struct Film: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Film"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("title", type: .nonNull(.scalar(String.self))),
          GraphQLField("episodeId", type: .nonNull(.scalar(Int.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(title: String, episodeId: Int) {
          self.init(unsafeResultMap: ["__typename": "Film", "title": title, "episodeId": episodeId])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// The title of this film
        public var title: String {
          get {
            return resultMap["title"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "title")
          }
        }

        /// The episode number of this film.
        public var episodeId: Int {
          get {
            return resultMap["episodeId"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "episodeId")
          }
        }
      }

      public struct Homeworld: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Planet"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String) {
          self.init(unsafeResultMap: ["__typename": "Planet", "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// The name of the planet
        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }

      public struct Starship: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Starship"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String) {
          self.init(unsafeResultMap: ["__typename": "Starship", "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// The name of this starship. The common name, such as "Death Star".
        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }

      public struct Vehicle: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Vehicle"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(name: String) {
          self.init(unsafeResultMap: ["__typename": "Vehicle", "name": name])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// The name of this vehicle. The common name, such as "Sand Crawler" or "Speeder bike".
        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }
      }
    }
  }
}

public final class CharacterListByFilmQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query CharacterListByFilm($id: ID!) {
      Film(id: $id) {
        __typename
        characters {
          __typename
          id
          name
          species {
            __typename
            name
          }
        }
      }
    }
    """

  public let operationName: String = "CharacterListByFilm"

  public var id: GraphQLID

  public init(id: GraphQLID) {
    self.id = id
  }

  public var variables: GraphQLMap? {
    return ["id": id]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("Film", arguments: ["id": GraphQLVariable("id")], type: .object(Film.selections)),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(film: Film? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "Film": film.flatMap { (value: Film) -> ResultMap in value.resultMap }])
    }

    public var film: Film? {
      get {
        return (resultMap["Film"] as? ResultMap).flatMap { Film(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "Film")
      }
    }

    public struct Film: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Film"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("characters", type: .list(.nonNull(.object(Character.selections)))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(characters: [Character]? = nil) {
        self.init(unsafeResultMap: ["__typename": "Film", "characters": characters.flatMap { (value: [Character]) -> [ResultMap] in value.map { (value: Character) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var characters: [Character]? {
        get {
          return (resultMap["characters"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Character] in value.map { (value: ResultMap) -> Character in Character(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Character]) -> [ResultMap] in value.map { (value: Character) -> ResultMap in value.resultMap } }, forKey: "characters")
        }
      }

      public struct Character: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Person"]

        public static let selections: [GraphQLSelection] = [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
          GraphQLField("name", type: .nonNull(.scalar(String.self))),
          GraphQLField("species", type: .list(.nonNull(.object(Species.selections)))),
        ]

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(id: GraphQLID, name: String, species: [Species]? = nil) {
          self.init(unsafeResultMap: ["__typename": "Person", "id": id, "name": name, "species": species.flatMap { (value: [Species]) -> [ResultMap] in value.map { (value: Species) -> ResultMap in value.resultMap } }])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var id: GraphQLID {
          get {
            return resultMap["id"]! as! GraphQLID
          }
          set {
            resultMap.updateValue(newValue, forKey: "id")
          }
        }

        /// The name of this person.
        public var name: String {
          get {
            return resultMap["name"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "name")
          }
        }

        public var species: [Species]? {
          get {
            return (resultMap["species"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Species] in value.map { (value: ResultMap) -> Species in Species(unsafeResultMap: value) } }
          }
          set {
            resultMap.updateValue(newValue.flatMap { (value: [Species]) -> [ResultMap] in value.map { (value: Species) -> ResultMap in value.resultMap } }, forKey: "species")
          }
        }

        public struct Species: GraphQLSelectionSet {
          public static let possibleTypes: [String] = ["Species"]

          public static let selections: [GraphQLSelection] = [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("name", type: .nonNull(.scalar(String.self))),
          ]

          public private(set) var resultMap: ResultMap

          public init(unsafeResultMap: ResultMap) {
            self.resultMap = unsafeResultMap
          }

          public init(name: String) {
            self.init(unsafeResultMap: ["__typename": "Species", "name": name])
          }

          public var __typename: String {
            get {
              return resultMap["__typename"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "__typename")
            }
          }

          /// The name of this species.
          public var name: String {
            get {
              return resultMap["name"]! as! String
            }
            set {
              resultMap.updateValue(newValue, forKey: "name")
            }
          }
        }
      }
    }
  }
}

public final class FilmListQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query FilmList {
      allFilms {
        __typename
        id
        title
        episodeId
      }
    }
    """

  public let operationName: String = "FilmList"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static let selections: [GraphQLSelection] = [
      GraphQLField("allFilms", type: .nonNull(.list(.nonNull(.object(AllFilm.selections))))),
    ]

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(allFilms: [AllFilm]) {
      self.init(unsafeResultMap: ["__typename": "Query", "allFilms": allFilms.map { (value: AllFilm) -> ResultMap in value.resultMap }])
    }

    public var allFilms: [AllFilm] {
      get {
        return (resultMap["allFilms"] as! [ResultMap]).map { (value: ResultMap) -> AllFilm in AllFilm(unsafeResultMap: value) }
      }
      set {
        resultMap.updateValue(newValue.map { (value: AllFilm) -> ResultMap in value.resultMap }, forKey: "allFilms")
      }
    }

    public struct AllFilm: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["Film"]

      public static let selections: [GraphQLSelection] = [
        GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
        GraphQLField("id", type: .nonNull(.scalar(GraphQLID.self))),
        GraphQLField("title", type: .nonNull(.scalar(String.self))),
        GraphQLField("episodeId", type: .nonNull(.scalar(Int.self))),
      ]

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(id: GraphQLID, title: String, episodeId: Int) {
        self.init(unsafeResultMap: ["__typename": "Film", "id": id, "title": title, "episodeId": episodeId])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var id: GraphQLID {
        get {
          return resultMap["id"]! as! GraphQLID
        }
        set {
          resultMap.updateValue(newValue, forKey: "id")
        }
      }

      /// The title of this film
      public var title: String {
        get {
          return resultMap["title"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "title")
        }
      }

      /// The episode number of this film.
      public var episodeId: Int {
        get {
          return resultMap["episodeId"]! as! Int
        }
        set {
          resultMap.updateValue(newValue, forKey: "episodeId")
        }
      }
    }
  }
}
