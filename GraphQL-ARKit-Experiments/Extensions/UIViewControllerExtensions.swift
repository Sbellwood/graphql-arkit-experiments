//
//  UIViewControllerExtensions.swift
//  GraphQL-ARKit-Experiments
//
//  Created by Skyler Bellwood on 6/19/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation
import UIKit

/*
 Extensions for the UIViewController super class
 */
extension UIViewController {
    
    // This convenience function provides a standard and consistent error alert that can be created with just a title and message. The function defines a default case of the UIAlertAction, but allows for the developer to send a custom UIAlertAction for presentation.
    func presentErrorAlert(title: String, message: String, action: UIAlertAction = UIAlertAction(title: "OK", style: .default)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(action)
        self.present(alert, animated: true)
    }
}
